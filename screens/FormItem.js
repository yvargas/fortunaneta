import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Platform, KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import { LinearGradient } from 'expo-linear-gradient';
import { Chevron } from 'react-native-shapes';
import { Ionicons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { connectActionSheet } from '@expo/react-native-action-sheet';
import { categories } from '../config/setup'

import * as firebase from 'firebase/app';
import 'firebase/firestore';


class Form extends React.Component {

  constructor(){
    super()
    this.state = {
      key : '0',
      title : '',
      category: '',
      value : '',
      color : '',
      placeholder : {
        label: 'Select a category...',
        value: null,
        color: '#9EA0A4',
      }
      
    }
  }

  saveItem = () => {
    if(this.state.title && this.state.category && this.state.value){
      
      let key = (this.props.items.length + 1).toString();
      let color = 'rgba(131, 167, 234, 1)'
      
      if(this.state.category === 'real-state'){
        color = 'rgba(100, 200, 76, 1)';
      } else if(this.state.category === 'vehicle'){
        color = 'rgba(222, 190, 75, 1)';
      } else if(this.state.category === 'loan'){
        color = 'rgba(200, 76, 76, 1)';
      } else if(this.state.category === 'savings'){
        color = 'rgba(50, 119, 168, 1)';
      } else if(this.state.category === 'assets'){
        color = 'rgba(168, 79, 227, 1)';
      }

      this.setState({ 
        key   : key,
        color : color
      }, () => {

        firebase
          .firestore()
          .collection('items')
          .add({
            title     : this.state.title,
            category  : this.state.category,
            value     : this.state.value,
            color     : this.state.color,
            uid       : this.props.profile.uid
          }).then(r => {

            this.props.addItem({
              key       : r.id,
              title     : this.state.title,
              category  : this.state.category,
              value     : this.state.value,
              color     : this.state.color,
              uid       : this.props.profile.uid
            });
          
          }).then(r => {

            this.props.navigation.navigate('Resume');
          
          });
      });

    } else {

      alert('Please fill all inputs');
    }

  }
  render(){
    return (
      <View style={styles.container}>
        <KeyboardAvoidingView behavior="position" enabled>
        
        <View style={styles.form}>
          <RNPickerSelect
            placeholder={this.state.placeholder}
            items={categories}
            onValueChange={(value,key,color) => {
              this.setState({
                category: value
              });
            }}
            style={{
              inputAndroid: {
                backgroundColor: 'transparent',
                padding: 10
              },
              inputIOS: {
                backgroundColor: 'transparent',
                padding: 10
              },
              iconContainer: {
                top: 15,
                right: 15,
              },
            }}
            value={this.state.category}
            useNativeAndroidPickerStyle={false}
            Icon={() => {
              return <Chevron size={1.5} color="gray" />;
            }}
          />
          <TextInput placeholder="Title" style={ styles.inputText } value={this.state.title} onChangeText={text => this.setState({ title: text })}/>
          <TextInput placeholder="Value" style={ styles.inputText } keyboardType="numeric" value={this.state.value} onChangeText={text => this.setState({ value: text })}/>
        </View>

        <TouchableOpacity onPress={()=>{this.saveItem()}} style={[styles.btn, {backgroundColor : 'green'}]}>
          <Text style={{color : '#fff'}}>Save</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={()=>{this.props.navigation.goBack()}} style={[styles.btn, {backgroundColor : '#002f50'}]}>
          <Text style={{color : '#fff'}}>Cancel</Text>
        </TouchableOpacity>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    items   : state.Neta.items,
    profile : state.Profile.profile 
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addItem : item => dispatch({type : 'ADD_ITEM', payload : item})
  }
}

const wrapper = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  connectActionSheet
);

export default wrapper(Form);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop : 25,
    // opacity : 0.7
    justifyContent: 'center',
    backgroundColor : '#79f89e'
  },
  form : {
    backgroundColor : '#fff',
    margin : 20,
  },
  inputText : {
    marginVertical : 10,
    paddingHorizontal : 10,

  },
  btn : {
    alignItems : 'center',
    justifyContent : 'center',
    height :44,

    // marginTop : 10,
    marginBottom : 20,
    marginHorizontal : 20,
    // borderWidth : 1
  }
});
