import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, StatusBar, Button, TouchableOpacity, FlatList, ImageBackground, ScrollView} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import { connect } from 'react-redux';
import Total from "../components/Total";

import PieChart from '../components/PieChart';

class Resume extends React.Component {

  constructor(){
    super();
    this.state = {
      lastTotal : 0,
      total     : 0
    }
  }

  componentDidMount = () => {
    
    this.props.navigation.addListener('didFocus', this.onScreenFocus)
  }

  onScreenFocus = () => {
    this.setState({
      total : this.getTotal()
    })
  }

  getData = () => {

    let data = [];

    this.props.items.forEach((i,k) => {

      let index = data.findIndex(item => item.name == i.category);

      if(index !== -1){

        data[index].population = parseFloat(data[index].population) + parseFloat(i.value)

      } else {

        data.push({
          name            : i.category,
          population      : parseFloat(i.value),
          color           : i.color,
          legendFontColor : '#7F7F7F',
          legendFontSize  : 14,
          id              : k.toString()
        });
      }
    });

    return data;
  }

  currencyFormat = (num) => {
    return "$" + num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  getIconName = category => {

    let iconName = 'ios-information';

    if(category == 'real-state'){

      iconName = 'ios-home';
    } else if(category == 'vehicle') {

      iconName = 'ios-car';
    } else if(category == 'loan') {

      iconName = 'ios-help-buoy';
      
    } else if(category == 'savings') {

      iconName = 'ios-business';
    } else if(category == 'assets') {

      iconName = 'ios-laptop';
    }
    return iconName;
  }

  getTotal = () => {

    return (this.getData().filter(i => i.name != 'loan').reduce((a, b) => a + b.population,0)) - (this.getData().filter(i => i.name == 'loan').reduce((a, b) => a + b.population,0));
  }

  getGoals = () => {

    goals = '';
    
    this.props.goals.forEach((g,k) => {

      goals += g.title + " " + this.currencyFormat(g.amount) + " | ";
    })

    return goals.substring(0, goals.length -2);
  }
  
  render(){

    return (

      <View style={styles.container}>
        <ImageBackground source={require('../assets/resume-bg.png')} imageStyle={styles.bgImage} style={[styles.header]}>
          <View>
            <Text style={styles.headerDescription}>Net Fotrune</Text>
            <Text style={styles.headerTitle}>{this.currencyFormat(this.state.total)}</Text>
          </View>
        </ImageBackground>

        <View style={styles.goals}>
          <ScrollView horizontal showsHorizontalScrollIndicator={false} style={styles.goalsBox}>
            {this.props.goals.map((g,k) => <TouchableOpacity key={k} style={[styles.goal, {backgroundColor : g.color}]} onPress={() => this.props.navigation.navigate('DetailGoals', {goalId:g.key})}><Text style={[styles.goalText,{color : '#565656'}]}>{g.title}</Text></TouchableOpacity>)}
          </ScrollView>
        </View>

        <View style={styles.pieBox}>
          <PieChart data={this.getData()} />
        </View>

        <View style={styles.separator}>
          <Text style={styles.categoryTitle}>Categories</Text>
        </View>

        <FlatList
          data={this.getData()}
          renderItem={({ item }) => 
            <View style={styles.item}>
              <View style={[styles.miniBoxItem,{ backgroundColor : item.color }]}>
                
              </View>
              
              <View style={styles.categoryBox}>
                
                <Text style={styles.categoryText}>
                   {item.name}
                </Text>
              </View>
              <View style={styles.moneyBox}>
                <Text style={styles.moneyText}>{this.currencyFormat(item.population)}</Text>
              </View>
            </View>
          }
          keyExtractor={item => item.id}
        />
        
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    items: state.Neta.items,
    goals: state.Goals.goals.filter(g => !g.done),
  };
};

export default connect(mapStateToProps)(Resume);

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
  },
  header : {
    height : 110,
    backgroundColor : '#002f50',
    paddingTop: 40,
    paddingHorizontal : 10,
    // padding : 10,
  },
  headerTitleBox : {
    // flex : 1
  },
  headerTitle : {
    textAlign : 'right',
    color : 'white',
    fontSize : 36
  },
  bgImage : {
    opacity : 0.4
  },
  headerDescription : {
    fontSize : 16,
    textAlign : 'right',
    color : '#ddd',
    paddingLeft : 20
  },
  item : {
    // marginVertical : 20,
    flexDirection : 'row',
    
  },
  iconItem : {
    // backgroundColor : 'red',
    width : 30,
    height : 50,
    // borderTopRightRadius : 50,
    // borderBottomRightRadius : 50,
    justifyContent : 'center',
    alignItems : 'center',
    // borderTopRightRadius : 20,
  },
  miniBoxItem : {
    // backgroundColor : 'red',
    width : 6,
    height : 50,
    // borderTopRightRadius : 50,
    // borderBottomRightRadius : 50,
    justifyContent : 'center',
    alignItems : 'center',
    // borderTopRightRadius : 20,
  },
  categoryBox : {
    flex: 1,
    height : 50,
    justifyContent : 'center',
    // backgroundColor : 'blue',
    // borderBottomWidth : 1,
    // borderColor : '#ddd'
  },
  moneyBox : {
    flex: 1,
    height : 50,
    justifyContent : 'center',
    // backgroundColor : 'red',
    // borderBottomWidth : 1,
    // borderColor : '#ddd'

  },
  moneyText : {
    textAlign : 'right',
    paddingRight : 10,
    // fontWeight : 'bold'
  },
  categoryText : {
    // fontWeight : 'bold',
    paddingLeft : 10,
  },
  pieBox : {
    backgroundColor : '#ddd'
    // borderWidth : 1,
    // borderColor : '#ddd',
    // marginBottom : 20,
    // elevation:1,
    // shadowOffset: { width: 30, height: 5 },
    // shadowColor: "yellow",
    // shadowOpacity: 0.5,
    // shadowRadius: 10,
  },
  separator : {
    alignItems : 'center',
    justifyContent : 'center',
    height : 40,
    backgroundColor : 'gray',
    // marginBottom : 10
  },
  categoryTitle : {
    fontWeight : 'bold',
    color : 'white'
  },
  objetivesBox : {
    borderTopWidth : 1,
    borderColor : '#01487b',
    backgroundColor : "#002540"
  },
  goals : {
    flexDirection : 'row'
  },
  goalsBox : {
    flexDirection : 'row',
    borderTopWidth : 1,
    borderColor : '#01487b',
    backgroundColor : "#002540"
  },
  goal : {
    marginLeft : 10,
    marginVertical : 10,
    paddingVertical : 4,
    paddingHorizontal : 20,
    backgroundColor : '#3e6380',
    borderRadius : 30
  },
  goalText : {
    color : 'white'
  }

});
