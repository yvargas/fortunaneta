import React from 'react';
import { StyleSheet, Text, View, StatusBar, Button} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import * as firebase from 'firebase/app';
import 'firebase/auth';

class Objetives extends React.Component {

  constructor(){
    super();
    this.state = {
      
    }
  }

  logout = async () => {
    try {
      await firebase.auth().signOut();
      this.props.navigation.navigate('Login');
    } catch (error) {
      alert('Unable to sign out right now');
    }
  }
  
  render(){
    return (
      <View style={styles.container}>
        <Text>El Objetives </Text>
        <Button title="Logout" onPress={this.logout}/>
      </View>
    );
  }
}

export default Objetives;

const styles = StyleSheet.create({
  container: {
    justifyContent : 'center',
    alignItems : 'center',
    flex: 1,
    backgroundColor: '#fff',
  }
});
