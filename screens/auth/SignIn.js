import React from 'react';
import { StyleSheet, Text, View, StatusBar, TextInput, Image, TouchableOpacity, ActivityIndicator, KeyboardAvoidingView, ImageBackground } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';
import { colors, theme }  from '../../assets/theme'

import * as firebase from 'firebase/app';
import 'firebase/auth';

class Loading extends React.Component {

  constructor(){
    super();
    this.state = {
      email : '',
      password : '',
      loading : false,
      form : 'signin'
    }
  }

  signIn = async() => {

    if(this.state.email && this.state.password){

      this.setState({ loading : true })

      try {
        const response = await firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password);
        
        if (response) {
          this.props.navigation.navigate('Resume');
        }
      } catch (error) {
        this.setState({ loading: false });
        switch (error.code) {
          case 'auth/user-not-found':
            alert('A user with that email does not exist. Try signing Up');
            break;
          case 'auth/invalid-email':
            alert('Please enter an email address');
        }
      }
    } else {

      alert ('Please fill fields')
    }
  }

  signUp = async() => {

    if (this.state.email && this.state.password) {
      this.setState({ loading: true });
      try {
        const response = await firebase
          .auth()
          .createUserWithEmailAndPassword(
            this.state.email,
            this.state.password
          );
        if (response) {
          this.setState({ loading: false });
          

          this.props.navigation.navigate('Loading');
          //automatically signs in the user
        }
      } catch (error) {
        this.setState({ loading: false });
        if (error.code == 'auth/email-already-in-use') {
          alert('User already exists.Try loggin in');
        }
        console.log(error);
      }
    } else {
      alert('Please enter email and password');
    }
  }
  
  render(){
    return (
      <ImageBackground source={require('../../assets/lluvia.jpg')} style={styles.imageContainer} >
        {
          (this.state.form == 'signin')? 
            <View style={ styles.container }>
              <KeyboardAvoidingView behavior="position" enabled>
              <Text style={styles.loginText}>Login</Text>
              <TextInput placeholder="Email" style={ styles.inputText } value={ this.state.email } onChangeText={ text => this.setState({ email: text }) }/>
              <TextInput placeholder="Password" style={ styles.inputText } secureTextEntry value={ this.state.password } onChangeText={ text => this.setState({ password: text }) }/>
              <TouchableOpacity onPress={this.signIn} style={ [theme.btnPrimary, { marginTop : 20 }] }>
                {this.state.loading ? (
                    <ActivityIndicator size="small" color={"white"} />
                  ) : (
                    <Text style={styles.whiteTxt}>Sign in</Text>
                  )
                }
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.setState({form : 'signup'})} style={ [theme.btnPrimary, { marginTop : 20 }] }>
                  <Text style={styles.whiteTxt}>Sign up</Text>
                </TouchableOpacity>
              </KeyboardAvoidingView>
            </View>
          :
            <View style={ styles.container }>
              <KeyboardAvoidingView behavior="position" enabled>
              <Text style={styles.loginText}>Sing up</Text>
              <TextInput placeholder="Email" style={ styles.inputText } value={ this.state.email } onChangeText={ text => this.setState({ email: text }) }/>
              <TextInput placeholder="Password" style={ styles.inputText } secureTextEntry value={ this.state.password } onChangeText={ text => this.setState({ password: text }) }/>
              <TouchableOpacity onPress={this.signUp} style={ [theme.btnPrimary, { marginTop : 20 }] }>
                {this.state.loading ? (
                    <ActivityIndicator size="small" color={"white"} />
                  ) : (
                    <Text style={styles.whiteTxt}>Sign Up</Text>
                  )
                }
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.setState({form : 'signin'})} style={ [theme.btnPrimary, { marginTop : 20 }] }>
                  <Text style={styles.whiteTxt}>Sign In</Text>
                </TouchableOpacity>
              </KeyboardAvoidingView>
            </View>
        }
      </ImageBackground>
    );
  }
}

export default Loading;

const styles = StyleSheet.create({
  container : {
    flex : 1,
    justifyContent : 'center',
    // backgroundColor : 'rgba(220,220,220, .5)',
    backgroundColor : 'rgba(0,37,64, .5)',
    paddingHorizontal : 40
  },
  imageContainer : {
    width: '100%',
    height: '100%',
  },
  inputText: {
    marginTop : 20,
    borderBottomWidth : 1,
    borderColor : 'white',
    marginBottom : 20,
    color : 'white'
  },
  loginText : {
    fontSize : 40,
    textAlign: 'center',
    color : 'white'
  },
  whiteTxt : {
    color : 'white'
  }
});
