import React from 'react';
import { StyleSheet, Text, View, StatusBar, ActivityIndicator } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';

import { connect } from 'react-redux';
import { compose } from 'redux';
import { connectActionSheet } from '@expo/react-native-action-sheet';

import moment from "moment";

import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

class Loading extends React.Component {

  componentDidMount() {
    this.checkIfLoggedIn();
  }

  getData = async uid => {

    var items = await firebase
      .firestore()
      .collection('items')
      .where('uid', '==', uid)
      .get();

    var goals = await firebase
      .firestore()
      .collection('goals')
      .where('uid', '==', uid)
      .get();

    var revenueDivision = await firebase
      .firestore()
      .collection('revenue-division')
      .doc(uid)
      .get();

    items           = items.docs.map(item => ({ key : item.id, ...item.data() }))
    goals           = goals.docs.map(goal => ({ 
      key     : goal.id, 
      amount  : goal.data().amount,
      color   : goal.data().color,
      date    : moment(goal.data().date.toDate()).format('YYYY-MM-DD'),
      done    : goal.data().done,
      title   : goal.data().title,
      uid     : goal.data().uid,

    }))

    this.props.loadItems(items)
    this.props.loadGoals(goals)
    this.props.loadRevenue(revenueDivision.data())

    this.props.loadProfile(firebase.auth().currentUser)

    this.props.navigation.navigate('Resume');
  }

  checkIfLoggedIn = async () => {
    this.unsubscribe = firebase.auth().onAuthStateChanged(user => {
      if (user) {

        // load data
        this.getData(user.uid)

      } else {
        //login screen
        this.props.navigation.navigate('SignIn');
      }
    });
  };

  componentWillUnmount() {
    this.unsubscribe();
  }

  render() {
    return (
      <LinearGradient style={styles.container} colors={['#002f50', '#5df364']}>
        <ActivityIndicator size="large" color={"gray"} />
      </LinearGradient>
    );
  }
}

const mapStateToProps = state => {
  return {
    items   : state.Neta.items,
    goals   : state.Goals.goals,
    revenue : state.Revenue.revenue,
    profile : state.Profile.Profile
  }
}

const mapDispatchToProps = dispatch => {
  return {
    loadItems   : items => dispatch({type : 'LOAD_ITEMS', payload : items}),
    loadGoals   : goals => dispatch({type : 'LOAD_GOALS', payload : goals}),
    loadRevenue : revenue => dispatch({type : 'LOAD_REVENUE', payload : revenue}),
    loadProfile : profile => dispatch({type : 'LOAD_PROFILE', payload : profile})
  }
}

const wrapper = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  connectActionSheet
);

export default wrapper(Loading);


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent : 'center',
    backgroundColor: '#fff',
  }
});
