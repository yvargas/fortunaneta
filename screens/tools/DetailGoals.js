import React from 'react';
import { StyleSheet, Text, View, StatusBar, Button,TouchableOpacity, KeyboardAvoidingView, TextInput} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { connectActionSheet } from '@expo/react-native-action-sheet';
import { SwipeListView } from 'react-native-swipe-list-view';

class DetailGoals extends React.Component {

  constructor(){
    super();
    this.state = {
      key       : '',
      title     : '',
      amount    : '',
      date      : '',
      done      : false,
      color     : '',
    }
  }

  componentDidMount = () => {

    let id    = this.props.navigation.state.params.goalId;
    let goal  = this.props.goals.filter(g => g.key === id); 

    this.setState({key,title,amount,date,done,color} = goal[0]);
  }

  currencyFormat = (num) => {
    return "$" + num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  render(){
    return (
      <View style={[styles.container, { backgroundColor : this.state.color}]}>
        <Text style={styles.txtGoal}>{this.state.title}</Text>
        <Text style={{}}>{this.currencyFormat(this.state.amount)}</Text>
        <Text style={{}}>{this.state.date}</Text>
      </View>
    );
  }
}


const mapStateToProps = state => {
  return {
    goals : state.Goals.goals
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addGoal : goal => dispatch({type : 'ADD_GOAL', payload : goal})
  }
}

const wrapper = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  connectActionSheet
);

export default wrapper(DetailGoals);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop : 50
  },
  txtGoal : {
    fontSize : 30,
    textAlign : 'center'
  }
});
