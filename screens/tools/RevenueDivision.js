import React from 'react';
import { StyleSheet, Text, View, StatusBar, Button, TextInput, TouchableOpacity, Keyboard} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { connect } from 'react-redux';

class RevenueDivision extends React.Component {

  constructor(){
    super();
    this.state = {
      amount      : '',
      showTable   : false,
      basics      : 0, 
      leisure     : 0, 
      training    : 0, 
      charity     : 0, 
      saving      : 0, 
      investment  : 0
    }
  }

  calculate = () => {

    if(this.state.amount > 0 ){

      let basicsPercent     = (this.props.revenue.basics.length == 1)? "0"+ this.props.revenue.basics : this.props.revenue.basics
      let leisurePercent    = (this.props.revenue.leisure.length == 1)? "0"+ this.props.revenue.leisure : this.props.revenue.leisure
      let trainingPercent   = (this.props.revenue.training.length == 1)? "0"+ this.props.revenue.training : this.props.revenue.training
      let charityPercent    = (this.props.revenue.charity.length == 1)? "0"+ this.props.revenue.charity : this.props.revenue.charity
      let savingPercent     = (this.props.revenue.saving.length == 1)? "0"+ this.props.revenue.saving : this.props.revenue.saving
      let investmentPercent = (this.props.revenue.investment.length == 1)? "0"+ this.props.revenue.investment : this.props.revenue.investment

      let basics      = this.state.amount * parseFloat(""+ 0.+"."+basicsPercent);
      let leisure     = this.state.amount * parseFloat(""+ 0.+"."+leisurePercent);
      let training    = this.state.amount * parseFloat(""+ 0.+"."+trainingPercent);
      let charity     = this.state.amount * parseFloat(""+ 0.+"."+charityPercent);
      let saving      = this.state.amount * parseFloat(""+ 0.+"."+savingPercent);
      let investment  = this.state.amount * parseFloat(""+ 0.+"."+investmentPercent);

      console.log(leisurePercent)
      console.log(trainingPercent)

      this.setState({
        basics      : basics,
        leisure     : leisure,
        training    : training,
        charity     : charity,
        saving      : saving,
        investment  : investment,
        showTable   : true,
        total       : basics + leisure + training + charity + saving + investment
      }, () => {
        Keyboard.dismiss()
      })
    }
  }

  render(){

    return (
      <View style={styles.container}>
        <TextInput style={ styles.inputAmount } placeholder={'Amount'} keyboardType="numeric" value={this.state.amount} onChangeText={text => this.setState({ amount: text })}/>
        <TouchableOpacity onPress={()=>{this.calculate()}} style={styles.btn}>
          <Text style={{color : '#fff'}}>Calculate</Text>
        </TouchableOpacity>

        {
          (this.state.showTable)?
            <View style={styles.table}>
              <View style={styles.tableRow}>
                <View style={styles.tableName}><Text style={styles.tableNameText}>Basics</Text></View>
                <View style={styles.tableAmount}><Text style={styles.tableAmountText}>{this.state.basics}</Text></View>
              </View>
              <View style={styles.tableRow}>
                <View style={styles.tableName}><Text style={styles.tableNameText}>Leisure</Text></View>
                <View style={styles.tableAmount}><Text style={styles.tableAmountText}>{this.state.leisure}</Text></View>
              </View>
              <View style={styles.tableRow}>
                <View style={styles.tableName}><Text style={styles.tableNameText}>Training</Text></View>
                <View style={styles.tableAmount}><Text style={styles.tableAmountText}>{this.state.training}</Text></View>
              </View>
              <View style={styles.tableRow}>
                <View style={styles.tableName}><Text style={styles.tableNameText}>Charity</Text></View>
                <View style={styles.tableAmount}><Text style={styles.tableAmountText}>{this.state.charity}</Text></View>
              </View>
              <View style={styles.tableRow}>
                <View style={styles.tableName}><Text style={styles.tableNameText}>Saving</Text></View>
                <View style={styles.tableAmount}><Text style={styles.tableAmountText}>{this.state.saving}</Text></View>
              </View>
              <View style={styles.tableRow}>
                <View style={styles.tableName}><Text style={styles.tableNameText}>Investment</Text></View>
                <View style={styles.tableAmount}><Text style={styles.tableAmountText}>{this.state.investment}</Text></View>
              </View>
              <View style={styles.tableRow}>
                <View style={styles.tableName}><Text style={styles.tableNameText}>Total</Text></View>
                <View style={styles.tableAmount}><Text style={styles.tableAmountText}>{this.state.total}</Text></View>
              </View>
            </View>
          :
            <View></View>

        }
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    revenue : state.Revenue.revenue
  };
};

export default connect(mapStateToProps)(RevenueDivision);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  inputAmount : {
    marginTop : 60,
    marginHorizontal : 60,
    borderBottomWidth : 1,
    borderColor : '#ddd',
    textAlign : 'center',
    fontSize : 40,
    paddingVertical : 22
  },
  textAmount : {
    marginTop: 6,
    textAlign : 'center',
    fontSize : 14,
  },
  btn : {
    marginTop : 10,
    alignItems : 'center',
    justifyContent : 'center',
    height :44,
    backgroundColor : '#002f50',

    // marginTop : 10,
    marginBottom : 20,
    marginHorizontal : 60,
    // borderWidth : 1
  },
  table : {
    marginTop : 60,
    
    paddingHorizontal : 20,
  },
  tableRow : {
    paddingVertical : 4,
    flexDirection : 'row',
    borderBottomWidth : 1,
    borderColor : '#ddd',
    marginBottom : 10

  },
  tableName : {
    flex : 1
  },
  tableAmount : {
    flex : 1

  },
  tableAmountText : {
    textAlign : 'right'
  }


});
