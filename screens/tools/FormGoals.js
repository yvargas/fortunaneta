import React from 'react';
import { StyleSheet, Text, View, StatusBar, Button,TouchableOpacity, KeyboardAvoidingView, TextInput, Dimensions} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { connectActionSheet } from '@expo/react-native-action-sheet';
import { SwipeListView } from 'react-native-swipe-list-view';
import DatePicker from 'react-native-datepicker';

import moment from "moment";

import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';


class FormGoals extends React.Component {

  constructor(){
    super();
    this.state = {
      date: '2020-04-06',
      key       : '',
      title     : '',
      amount    : '',
      date      : '',
      done      : false,
      color     : 'white',
    }
  }

  saveGoal = () => {

    if(this.state.date && this.state.title && this.state.amount){

      firebase
        .firestore()
        .collection('goals')
        .add({
          title     : this.state.title,
          amount    : this.state.amount,
          date      : new Date(this.state.date),
          done      : false,
          color     : this.state.color,
          uid       : this.props.profile.uid
        }).then(r => {

          this.props.addGoal({
            key       : r.id,
            title     : this.state.title,
            amount    : this.state.amount,
            date      : this.state.date,
            done      : false,
            color     : this.state.color,
            uid       : this.props.profile.uid
          });
        
        }).then(r => {

          this.props.navigation.navigate('Resume');
        
        });

    } else {

      alert('Please fill all inputs');
    }

  }

  changeColor = (color) => {
    this.setState({
      color : color
    })
  }

  render(){
    return (
      <View style={[styles.container, {backgroundColor: this.state.color}]}>
        <KeyboardAvoidingView behavior="position" enabled>
            
          <View style={styles.containerColors}>
            <TouchableOpacity onPress={() => this.changeColor('white')}><View style={[styles.colorBox, styles.colorWhite, (this.state.color == 'white')? styles.colorSelected : '']} ></View></TouchableOpacity>
            <TouchableOpacity onPress={() => this.changeColor('rgb(255, 105, 97)')}><View style={[styles.colorBox, styles.colorRed, (this.state.color == 'rgb(255, 105, 97)')? styles.colorSelected : '']} ></View></TouchableOpacity>
            <TouchableOpacity onPress={() => this.changeColor('rgb(157, 186, 213)')}><View style={[styles.colorBox, styles.colorBlue, (this.state.color == 'rgb(157, 186, 213)')? styles.colorSelected : '']} ></View></TouchableOpacity>
            <TouchableOpacity onPress={() => this.changeColor('rgb(250, 243, 221)')}><View style={[styles.colorBox, styles.colorYellow, (this.state.color == 'rgb(250, 243, 221)')? styles.colorSelected : '']} ></View></TouchableOpacity>
            <TouchableOpacity onPress={() => this.changeColor('rgb(193, 187, 221)')}><View style={[styles.colorBox, styles.colorPurple, (this.state.color == 'rgb(193, 187, 221)')? styles.colorSelected : '']} ></View></TouchableOpacity>
            <TouchableOpacity onPress={() => this.changeColor('rgb(76, 224, 131)')}><View style={[styles.colorBox, styles.colorGreen, (this.state.color == 'rgb(76, 224, 131)')? styles.colorSelected : '']} ></View></TouchableOpacity>
          </View>

          <View style={styles.form}>
            <TextInput placeholder="Title" style={ styles.inputText } value={this.state.title} onChangeText={text => this.setState({ title: text })}/>
            <TextInput placeholder="Value" style={ styles.inputText } keyboardType="numeric" value={this.state.amount} onChangeText={text => this.setState({ amount: text })}/>
            <DatePicker
              style={{ width : Dimensions.get('window').width - 40 , margin : 20 }}
              date={this.state.date} //initial date from state
              mode="date" //The enum of date, datetime and time
              placeholder="Select date"
              format="YYYY-MM-DD"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              showIcon={false}
              customStyles={{
                dateInput: {
                  // margin : 20,
                  fontSize : 22,
                  borderTopWidth: 0,
                  borderLeftWidth: 0,
                  borderRightWidth: 0,
                  borderBottomWidth : 1,
                  borderColor : '#ddd',
                  color : '#ddd',
                },
              }}
              onDateChange={date => {
                this.setState({ date: date });
              }}
            />
          </View>

          <TouchableOpacity onPress={()=>{this.saveGoal()}} style={[styles.btn, {backgroundColor : 'green'}]}>
            <Text style={{color : '#fff'}}>Save</Text>
          </TouchableOpacity>
        
        </KeyboardAvoidingView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    goals : state.Goals.goals,
    profile : state.Profile.profile 
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addGoal : goal => dispatch({type : 'ADD_GOAL', payload : goal})
  }
}

const wrapper = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  connectActionSheet
);

export default wrapper(FormGoals);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop : 30
  },
  inputText : {
    margin : 20,
    textAlign : 'center',
    fontSize : 20,
    borderBottomWidth : 1,
    borderColor : '#ddd',
    color : 'black'
  },
  btn : {
    alignItems : 'center',
    justifyContent : 'center',
    height :44,

    // marginTop : 10,
    marginBottom : 20,
    marginHorizontal : 20,
    // borderWidth : 1
  },
  containerColors : {
    paddingHorizontal : 20,
    // justifyContent : 'center',
    flexDirection : 'row',
    // borderColor : 'red',
    // borderWidth : 1,
    height : 44
  },
  colorBox : {
    marginRight : 20,
    width : 44,
    height : 44,
    borderRadius : 44
  },
  colorWhite : { backgroundColor : 'white' },
  colorBlue   : { backgroundColor : 'rgb(157, 186, 213)' },
  colorRed    : { backgroundColor : 'rgb(255, 105, 97)'  },
  colorYellow : { backgroundColor : 'rgb(250, 243, 221)' },
  colorPurple : { backgroundColor : 'rgb(193, 187, 221)' },
  colorGreen : { backgroundColor : 'rgb(76, 224, 131)' },
  colorSelected : { borderColor : 'black', borderWidth : 3 }
});
