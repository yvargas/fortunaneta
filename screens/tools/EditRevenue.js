import React from 'react';
import { StyleSheet, Text, View, StatusBar, Button, TextInput, TouchableOpacity, AppState} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { connectActionSheet } from '@expo/react-native-action-sheet';

import * as firebase from 'firebase/app';
import 'firebase/firestore';

class RevenueDivision extends React.Component {

  constructor(){
    super();
    this.state = {
      total       : 0,
      basics      : 0, 
      leisure     : 0, 
      training    : 0, 
      charity     : 0, 
      saving      : 0, 
      investment  : 0
    }
  }

  componentDidMount = () => {
    const {basics, leisure, training, charity, saving, investment} = this.props.revenue;

    this.setState({
      basics      : basics, 
      leisure     : leisure, 
      training    : training, 
      charity     : charity, 
      saving      : saving, 
      investment  : investment,
      total       : parseInt(basics) + parseInt(leisure) + parseInt(training) + parseInt(charity) + parseInt(saving) + parseInt(investment)
    })
  }

  updateRevenue = (type, percent) => {

    if(percent == ""){
      percent = 0;
    } else if(percent.length == 2 && percent.charAt(0) == "0"){
      percent = percent.substring(1)
    }

    this.setState({[type] : percent}, () => {
      this.setState({
        total : parseInt((this.state.basics == '')? 0 : this.state.basics) + parseInt((this.state.leisure == '')? 0 : this.state.leisure) + parseInt((this.state.training == '')? 0 : this.state.training) + parseInt((this.state.charity == '')? 0 : this.state.charity) + parseInt((this.state.saving == '')? 0 : this.state.saving) + parseInt((this.state.investment == '')? 0 : this.state.investment)
      })
    })
  }

  updateNewRevenue = () => {

    firebase
      .firestore()
      .collection('revenue-division')
      .doc(this.props.profile.uid)
      .update({basics,leisure,training,charity,saving,investment,total} = this.state)
      .then(r => {

        this.props.updateRevenueModel({basics,leisure,training,charity,saving,investment,total} = this.state);

      }).then(r => {

        this.props.navigation.navigate('RevenueDivision');
      })
  }

  render(){
    return (
      <View style={styles.container}>
        <View style={styles.infoBox}>
          <Ionicons name={'ios-information-circle-outline'} color={'black'} size={22}/><Text> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis et ultricies odio, et cursus elit. Donec dui neque, rutrum in sem id, laoreet cursus leo. Quisque dignissim fermentum mi, quis semper arcu gravida id.</Text>
        </View>

        <View style={styles.table}>
          <View style={styles.tableRow}>
            <View style={styles.tableName}><Text style={styles.tableNameText}>Basics</Text></View>
            <View style={styles.tableAmount}><TextInput style={styles.tableAmountText} value={this.state.basics.toString()} keyboardType="numeric" onChangeText={value => this.updateRevenue('basics', value )}/></View>
            <View style={styles.tablePercent}><Text style={styles.tableNameText}>%</Text></View>
          </View>
          <View style={styles.tableRow}>
            <View style={styles.tableName}><Text style={styles.tableNameText}>Leisure</Text></View>
            <View style={styles.tableAmount}><TextInput style={styles.tableAmountText} value={this.state.leisure.toString()} keyboardType="numeric" onChangeText={value => this.updateRevenue('leisure', value )}/></View>
            <View style={styles.tablePercent}><Text style={styles.tableNameText}>%</Text></View>
          </View>
          
          <View style={styles.tableRow}>
            <View style={styles.tableName}><Text style={styles.tableNameText}>Training</Text></View>
            <View style={styles.tableAmount}><TextInput style={styles.tableAmountText} value={this.state.training.toString()} keyboardType="numeric" onChangeText={value => this.updateRevenue('training', value )}/></View>
            <View style={styles.tablePercent}><Text style={styles.tableNameText}>%</Text></View>
          </View>
          
          <View style={styles.tableRow}>
            <View style={styles.tableName}><Text style={styles.tableNameText}>Charity</Text></View>
            <View style={styles.tableAmount}><TextInput style={styles.tableAmountText} value={this.state.charity.toString()} keyboardType="numeric" onChangeText={value => this.updateRevenue('charity', value)}/></View>
            <View style={styles.tablePercent}><Text style={styles.tableNameText}>%</Text></View>
          </View>
          
          <View style={styles.tableRow}>
            <View style={styles.tableName}><Text style={styles.tableNameText}>Saving</Text></View>
            <View style={styles.tableAmount}><TextInput style={styles.tableAmountText} value={this.state.saving.toString()} keyboardType="numeric" onChangeText={value => this.updateRevenue('saving', value)}/></View>
            <View style={styles.tablePercent}><Text style={styles.tableNameText}>%</Text></View>
          </View>
          
          <View style={styles.tableRow}>
            <View style={styles.tableName}><Text style={styles.tableNameText}>Investment</Text></View>
            <View style={styles.tableAmount}><TextInput style={styles.tableAmountText} value={this.state.investment.toString()} keyboardType="numeric" onChangeText={value => this.updateRevenue('investment', value)}/></View>
            <View style={styles.tablePercent}><Text style={styles.tableNameText}>%</Text></View>
          </View>

          <View style={styles.tableRowTotal}>
            <View style={styles.tableName}><Text style={[styles.tableNameText, (this.state.total != 100)? {color : 'red'} : {}]}>Total</Text></View>
            <View style={styles.tableAmount}><Text style={[styles.tableAmountText, (this.state.total != 100)? {color : 'red'} : {} ]}>{this.state.total.toString()}</Text></View>
            <View style={styles.tablePercent}><Text style={[styles.tableNameText, (this.state.total != 100)? {color : 'red'} : {}]}>%</Text></View>
          </View>
          
        </View>

        {
          (this.state.total == 100)?
            <TouchableOpacity onPress={()=>{this.updateNewRevenue()}} style={[styles.btn, {backgroundColor : 'green'}]}>
              <Text style={{color : '#fff'}}>Update</Text>
            </TouchableOpacity>
          : 
            <View style={styles.messageBox}>
              <Text style={styles.messageBoxTxt}>Please check your percentage</Text>
            </View>
        }
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    revenue : state.Revenue.revenue,
    profile : state.Profile.profile
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateRevenueModel : revenue => dispatch({type : 'UPDATE_DIVISIONS', payload : revenue})
  }
}

const wrapper = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  connectActionSheet
);

export default wrapper(RevenueDivision);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  infoBox : {
    backgroundColor : 'yellow',
    paddingHorizontal : 10,
    paddingVertical : 20,
    flexDirection : 'row'
  },
  inputAmount : {
    marginTop : 60,
    marginHorizontal : 60,
    borderBottomWidth : 1,
    borderColor : '#ddd',
    textAlign : 'center',
    fontSize : 40,
    paddingVertical : 22
  },
  textAmount : {
    marginTop: 6,
    textAlign : 'center',
    fontSize : 14,
  },
  btn : {
    marginTop : 10,
    alignItems : 'center',
    justifyContent : 'center',
    height :44,
    backgroundColor : '#002f50',

    // marginTop : 10,
    marginBottom : 20,
    marginHorizontal : 60,
    // borderWidth : 1
  },
  table : {
    marginTop : 60,
    
    paddingHorizontal : 20,
  },
  tableRow : {
    paddingVertical : 4,
    flexDirection : 'row',
    borderBottomWidth : 1,
    borderColor : '#ddd',
    paddingBottom : 10

  },
  tableRowTotal : {
    paddingVertical : 4,
    flexDirection : 'row',
    borderTopWidth : 1,
    borderColor : '#ddd',
    marginBottom : 10

  },
  tableName : {
    flex : 1
  },
  tableAmount : {
    flex : 1

  },
  tableAmountText : {
    textAlign : 'right',
    paddingRight : 4
  },
  tablePercent : {
    // width : 10
  },
  btn : {
    alignItems : 'center',
    justifyContent : 'center',
    height :44,

    marginTop : 20,
    marginBottom : 20,
    marginHorizontal : 20,
    // borderWidth : 1
  },
  messageBox : {
    paddingTop : 30,
    alignItems : 'center'
  },
  messageBoxTxt : {
    color : 'red'

  }


});
