import React from 'react';
import { StyleSheet, Text, View, StatusBar, Button,TouchableOpacity} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { connectActionSheet } from '@expo/react-native-action-sheet';
import { SwipeListView } from 'react-native-swipe-list-view';

import * as firebase from 'firebase/app';
import 'firebase/firestore';

class Goals extends React.Component {

  constructor(){
    super();
    this.state = {
      
    }
  }
  currencyFormat = num => {
    return '$' + num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  changeStatus = item => {    

    firebase
      .firestore()
      .collection('goals')
      .doc(item.key)
      .update({ done : !item.done })
      .then(r => {

        this.props.changeStatusGoalReduce(item);

      }).then(r => {

        this.props.navigation.navigate('Resume')
      })
  }

  removeItem = item => {

    firebase
      .firestore()
      .collection('goals')
      .doc(item.key)
      .delete()
      .then(r => {

        this.props.removeItemGoalReduce(item);
      })
  }

  render(){
    return (
      <View style={styles.container}>
        <SwipeListView
          useFlatList={true}
          data={this.props.goals}
          renderItem={ (rowData, rowMap) => (
            <TouchableOpacity activeOpacity={1} style={styles.row} onPress={() => this.props.navigation.navigate('DetailGoals', {goalId : rowData.item.key })}>
              <View style={styles.firstBox}>
                <Text style={styles.goalText}>{rowData.item.title}</Text>
              </View>
              <View style={styles.secondBox}>
                <View style={styles.box}>
                  <Text style={[ styles.goalDetail, { textAlign: 'left' }]}>{rowData.item.date}</Text>
                </View>
                <View style={styles.box}>
                  <Text style={[ styles.goalDetail, { textAlign: 'right' }]}>{this.currencyFormat(rowData.item.amount)}</Text>
                </View>
              </View>
            </TouchableOpacity>
          )}
          renderHiddenItem={ (rowData, rowMap) => (
            <View style={styles.rowBack}>
              {
                (!rowData.item.done)?
                  <TouchableOpacity style={styles.doneBtn} onPress={ () => this.changeStatus(rowData.item) }>
                    <Ionicons name="ios-checkmark-circle" color={'#fff'} size={22} />
                  </TouchableOpacity>
                : 
                  <TouchableOpacity style={styles.notDoneBtn} onPress={ () => this.changeStatus(rowData.item) }>
                    <Ionicons name="ios-close-circle" color={'#fff'} size={22} />
                  </TouchableOpacity>
              }
              <TouchableOpacity style={styles.removeBtn} onPress={ () => this.removeItem(rowData.item) }>
                <Ionicons name="ios-trash" color={'#fff'} size={22} />
              </TouchableOpacity>
            </View>
          )}
          rightOpenValue={-150}
        />
      </View>
    );
  }
}


const mapStateToProps = state => {
  return {
    goals : state.Goals.goals
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addGoal                 : item => dispatch({type : 'ADD_ITEM', payload : goal}),
    changeStatusGoalReduce  : item => dispatch({type : 'CHANGE_STATUS', payload : item}),
    removeItemGoalReduce    : item => dispatch({type : 'REMOVE_GOAL', payload : item}),
  }
}

const wrapper = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  connectActionSheet
);

export default wrapper(Goals);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  itemBox : {
    margin : 10,
    paddingVertical : 4,
    paddingHorizontal : 20,
    backgroundColor : '#3e6380',
    borderRadius : 30
  },
  listItems : {
    // flex : 1
    // height : Dimensions.get('window').height - 334
  },
  row: {
    // flexDirection : 'row',
    // alignItems: 'center',
    backgroundColor: '#fff',
    // height: 50,
    padding : 10,
    borderBottomWidth : 1,
    borderColor : '#f2eeee',
  },
  rowIcon : {
    paddingRight : 10,
  },
  removeBtn: {
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    width: 75,
    right: 0,
    backgroundColor: '#ff6868',
  },
  doneBtn: {
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    width: 75,
    right: 75,
    backgroundColor: 'green',
  },
  notDoneBtn: {
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    width: 75,
    right: 75,
    backgroundColor: 'gray',
  },
  rowBack: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    // flexDirection: 'row',
  },
  firstBox : {
    flex: 1,

  },
  secondBox : {
    flex: 1,
    flexDirection : 'row',
    paddingTop: 4
  },
  box : {
    flex: 1,
    justifyContent : 'center'
  },
  goalText: {
    color : 'black',
    fontSize : 18
  },
  goalDetail: {
    color : '#737373'
  }
});
