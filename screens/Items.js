import React from 'react';
import { StyleSheet, Text, View, StatusBar, TouchableOpacity, ScrollView, Dimensions, Button} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { SwipeListView } from 'react-native-swipe-list-view';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { connectActionSheet } from '@expo/react-native-action-sheet';

import Header from '../components/Header';

import * as firebase from 'firebase/app';
import 'firebase/firestore';

class Items extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      filter : 'all'
    }
  }

  setFilter = (name) => {

    this.setState({ filter : name });

  }

  currencyFormat = num => {
    return '$ ' + num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }

  getIconName = category => {

    let iconName = 'ios-information';

    if(category == 'real-state'){

      iconName = 'ios-home';
    } else if(category == 'vehicle') {

      iconName = 'ios-car';
    } else if(category == 'loan') {

      iconName = 'ios-help-buoy';
      
    } else if(category == 'savings') {

      iconName = 'ios-business';
    } else if(category == 'assets') {

      iconName = 'ios-laptop';
    }
    return iconName;
  }

  removeItem = item => {

    firebase
      .firestore()
      .collection('items')
      .doc(item.key)
      .delete()
      .then(r => {

        this.props.removeItemReduce(item);
      })

  }
  
  render(){
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.headerTitleBox}>
            <Text style={styles.headerTitle}>Items</Text>
            <Text style={styles.headerDescription}>description</Text>
          </View>
          <TouchableOpacity style={styles.headerTitleBtn} onPress={() => this.props.navigation.navigate('FormItem')}>
            <Ionicons name={'ios-add'} color={'white'} size={22} />
          </TouchableOpacity>
        </View>
        <View style={styles.tags}>
          <ScrollView horizontal showsHorizontalScrollIndicator={false} style={styles.filterBox}>
            <TouchableOpacity style={[styles.itemBox, (this.state.filter == 'all'? styles.activeTag : {} )]} onPress={() => this.setFilter('all')}><Text>All</Text></TouchableOpacity>
            <TouchableOpacity style={[styles.itemBox, (this.state.filter == 'real-state'? styles.activeTag : {} )]} onPress={() => this.setFilter('real-state')}><Text>Real State</Text></TouchableOpacity>
            <TouchableOpacity style={[styles.itemBox, (this.state.filter == 'vehicle'? styles.activeTag : {} )]} onPress={() => this.setFilter('vehicle')}><Text>Vehicle</Text></TouchableOpacity>
            <TouchableOpacity style={[styles.itemBox, (this.state.filter == 'loan'? styles.activeTag : {} )]} onPress={() => this.setFilter('loan')}><Text>Loan</Text></TouchableOpacity>
            <TouchableOpacity style={[styles.itemBox, (this.state.filter == 'savings'? styles.activeTag : {} )]} onPress={() => this.setFilter('savings')}><Text>Savings</Text></TouchableOpacity>
            <TouchableOpacity style={[styles.itemBox, (this.state.filter == 'assets'? styles.activeTag : {} )]} onPress={() => this.setFilter('assets')}><Text>Assets</Text></TouchableOpacity>
          </ScrollView>
        </View>
        <View style={styles.listItems}>
          <SwipeListView
            useFlatList={true}
            data={(this.state.filter == 'all')? this.props.items : this.props.items.filter(i => i.category == this.state.filter)}
            renderItem={ (rowData, rowMap) => (
              <View style={styles.row}>
                <View style={{flexDirection : 'row', paddingHorizontal : 10}}>
                  <Ionicons name={this.getIconName(rowData.item.category)} color={'gray'} size={22} style={styles.rowIcon}/>
                  <View style={styles.box}>
                    <Text>{rowData.item.title}</Text>
                  </View>
                  <View style={styles.box}>
                    <Text style={{ textAlign: 'right' }}>{this.currencyFormat(rowData.item.value)}</Text>
                  </View>
                </View>
              </View>
            )}
            renderHiddenItem={ (rowData, rowMap) => (
              <View style={styles.rowBack}>
                <TouchableOpacity style={styles.removeBtn} onPress={ () => this.removeItem(rowData.item) }>
                  <Ionicons name="ios-trash" color={'#fff'} size={22} />
                </TouchableOpacity>
              </View>
            )}
            rightOpenValue={-75}
          />
        </View>
        
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    items   : state.Neta.items,
    profile : state.Profile.profile
  }
}

const mapDispatchToProps = dispatch => {

  return {
    removeItemReduce: item => dispatch({ type: 'REMOVE_ITEM', payload: item }),
  };
};

const wrapper = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  connectActionSheet
);

export default wrapper(Items);

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
  },
  header : {
    height : 150,
    backgroundColor : '#002f50',
    paddingTop: 70,
    flexDirection: 'row',
    alignContent: 'space-between',
  },
  headerTitleBox : {
    paddingLeft : 10,
    flex : 1
  },
  headerTitleBtn : {
    width : 50,
    height : 44,
    alignItems : 'center',
    justifyContent : 'center',
    backgroundColor : '#79f89e',
    borderTopLeftRadius : 30,
    borderBottomLeftRadius : 30,
  },
  headerTitle : {
    color : 'white',
    fontSize : 30
  },
  headerDescription : {
    fontSize : 14,
    color : '#ddd'
  },
  tags : {
    flexDirection : 'row'
  },
  activeTag : {
    borderWidth : 1,
    borderColor : '#79f89e'
  },
  filterBox : {
    flexDirection : 'row',
    borderTopWidth : 1,
    borderColor : '#01487b',
    backgroundColor : "#002540"
  },
  formItemBtn : {
    width : 80,
    backgroundColor : 'green',
    justifyContent : 'center',
    alignItems : 'center'
  },
  itemBox : {
    marginLeft : 10,
    marginVertical : 10,
    paddingVertical : 4,
    paddingHorizontal : 20,
    backgroundColor : '#3e6380',
    borderRadius : 30
  },
  listItems : {
    // flex : 1
    // height : Dimensions.get('window').height - 334
  },
  row: {
    flexDirection : 'row',
    alignItems: 'center',
    backgroundColor: '#fff',
    height: 50,
    borderBottomWidth : 1,
    borderColor : '#f2eeee',
  },
  rowIcon : {
    paddingRight : 10,
  },
  removeBtn: {
    alignItems: 'center',
    bottom: 0,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    width: 75,
    right: 0,
    backgroundColor: '#ff6868',
  },
  rowBack: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    flexDirection: 'row',
  },
  box : {
    flex: 1,
    justifyContent : 'center'
  }
});
