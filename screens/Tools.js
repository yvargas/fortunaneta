import React from 'react';
import { StyleSheet, Text, View, StatusBar, FlatList, SafeAreaView, TouchableOpacity} from 'react-native';
import { Ionicons } from '@expo/vector-icons';

class Tools extends React.Component {

  constructor(){
    super();
    this.state = {
      list : [
        {
          id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
          title: 'Goals',
          screen: 'Goals'
        },
        {
          id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
          title: 'Revenue division',
          screen : 'RevenueDivision'
        }
      ]
    }
  }

  render(){
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.headerTitleBox}>
            <Text style={styles.headerTitle}>Tools</Text>
          </View>
        </View>
        <FlatList
          data={this.state.list}
          renderItem={({ item }) => 
            <TouchableOpacity onPress={() => this.props.navigation.navigate(item.screen)}>
              <View style={styles.item}>
                <Text>{item.title}</Text>
              </View>
            </TouchableOpacity>
          }
          keyExtractor={item => item.id}
        />
      </View>
    );
  }
}

export default Tools;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header : {
    height : 197,
    backgroundColor : '#002f50',
    paddingTop: 100,
    flexDirection: 'row',
    alignContent: 'space-between',
  },
  headerTitleBox : {
    paddingLeft : 10,
    flex : 1
  },
  headerTitleBtn : {
    width : 50,
    height : 44,
    alignItems : 'center',
    justifyContent : 'center',
    backgroundColor : '#79f89e',
    borderTopLeftRadius : 30,
    borderBottomLeftRadius : 30,
  },
  headerTitle : {
    textAlign : 'center',
    color : 'white',
    fontSize : 30
  },
  headerDescription : {
    fontSize : 14,
    color : '#ddd'
  },
  item: {
    padding: 20,
    borderBottomWidth : 1,
    borderColor : '#ddd'
  }
});
