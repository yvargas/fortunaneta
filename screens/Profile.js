import React from 'react';
import { StyleSheet, Text, View, StatusBar, Button, TouchableOpacity} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';

import { connect } from 'react-redux';
import { compose } from 'redux';
import { connectActionSheet } from '@expo/react-native-action-sheet';

import * as firebase from 'firebase/app';
import 'firebase/auth';

class Profile extends React.Component {

  constructor(){
    super();
    this.state = {
      initial : ''
    }
  }

  componentDidMount = () => {

    this.getInitial();
  }

  getInitial = () => {

    this.setState({ initial : this.props.profile.email.toUpperCase().substring(0,1) })

  }
  
  logout = async () => {
    try {
      await firebase.auth().signOut();
      this.props.navigation.navigate('SignIn');
    } catch (error) {
      alert('Unable to sign out right now');
    }
  }
  
  render(){
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View>
            <View style={styles.headerBtn}>
            
            </View>
          </View>
          <View>
            <TouchableOpacity onPress={() => this.logout() } style={[styles.headerBtn]}>
              <Ionicons name="ios-power" color={'#fff'} size={22} />
            </TouchableOpacity>
          </View>
        </View>
        <Text>
          {this.props.profile.email}
        </Text>
        <View style={{alignItems : 'center'}}>
          <View style={styles.initial}><Text style={{ fontSize: 30, fontWeight : 'bold' }}>{this.state.initial}</Text></View>
        </View>
        <Button title="Logout" onPress={this.logout}/>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    profile : state.Profile.profile
  }
}

const mapDispatchToProps = dispatch => {

  return {
  };
};

const wrapper = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  connectActionSheet
);

export default wrapper(Profile);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  header : {
    paddingTop : 30,
    height : 150,
    flexDirection : 'row',
    justifyContent : 'space-between',
    backgroundColor : '#002f50'
  },
  headerBtn : {
    width: 44, 
    height:70,
    alignItems : 'center',
    justifyContent : 'center',
    // backgroundColor : 'red'

  },
  initial : {
    alignItems : 'center',
    justifyContent : 'center',
    marginTop : -50,
    borderWidth : 1,
    borderColor : '#ddd',
    borderRadius : 100,
    width : 100,
    height : 100,
    backgroundColor : '#ddd',
  }
});
