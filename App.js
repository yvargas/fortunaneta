import React from 'react';
import { StyleSheet, Text, View, StatusBar, Platform } from 'react-native';

import AppNavigator from './navigation/AppNavigator';
import { Provider } from 'react-redux';
import store from './redux/store';
import { ActionSheetProvider } from '@expo/react-native-action-sheet';

import * as firebase from 'firebase/app';
import FirebaseConfig from './config/FirebaseConfig';

export default class App extends React.Component {

	constructor(){
		super();

		this.initializeFirebase();
	}

	initializeFirebase = () => {
		firebase.initializeApp(FirebaseConfig);
	}

	render() {
		return (
		    <Provider store={store}>
		      <View style={styles.container}>
		      	<StatusBar barStyle="light-content" />
		        <ActionSheetProvider>
		          <AppNavigator />
		        </ActionSheetProvider>
		      </View>
		    </Provider>
	  	);	
	}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  }
});
