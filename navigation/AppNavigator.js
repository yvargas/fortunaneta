import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  FlatList
} from 'react-native';

import {
  createAppContainer,
  createSwitchNavigator,
} from 'react-navigation';

import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createDrawerNavigator } from 'react-navigation-drawer';


import { Ionicons } from '@expo/vector-icons';

import SignIn from '../screens/auth/SignIn';
import SignUp from '../screens/auth/SignUp';
import Loading from '../screens/auth/Loading';
import Resume from '../screens/Resume';
import Items from '../screens/Items';
import FormItem from '../screens/FormItem';
import Objetives from '../screens/Objetives';
import Tools from '../screens/Tools';

import Profile from '../screens/Profile';

// Tools
import Goals from '../screens/tools/Goals';
import FormGoals from '../screens/tools/FormGoals';
import DetailGoals from '../screens/tools/DetailGoals';
import RevenueDivision from '../screens/tools/RevenueDivision';
import EditRevenue from '../screens/tools/EditRevenue';


const AuthStackNavigator = createStackNavigator(
  {
    SignIn,
    SignUp
  },
  {
    mode: 'modal',
    defaultNavigationOptions: {
      header : null
    }
  }
);

const HomeTabNavigator = createBottomTabNavigator(
  {
    Items: {
      screen: Items,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Ionicons
            name="ios-list"
            size={22}
            color={tintColor}
          />
        )
      }
    },
    Resume: {
      screen: Resume,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Ionicons
            name="ios-today"
            size={22}
            color={tintColor}
          />
        )
      }
    },
    Tools: {
      screen: Tools,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Ionicons
            name="ios-construct"
            size={22}
            color={tintColor}
          />
        )
      }
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Ionicons
            name="ios-contact"
            size={22}
            color={tintColor}
          />
        )
      }
    }
  },
  {
    tabBarOptions: {
      showLabel:false,
      style: {
        backgroundColor: 'white'
      },
      activeTintColor: 'green',
      inactiveTintColor: 'gray'
    }
  }
);

const HomeStackNavigator = createStackNavigator(
  {
    HomeTabNavigator: {
      screen: HomeTabNavigator,
      navigationOptions: ({ navigation }) => {
        return {
          header : null
        };
      }
    },
    Profile: {
      screen: Profile,
      navigationOptions: ({ navigation }) => {
        return {
          header: null
        };
      }
    },
    Goals: {
      screen: Goals,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle : 'Goals',
          headerLeft: (
            <TouchableOpacity onPress={() => navigation.goBack()} style={styles.btnHeader}>
              <Ionicons
                name="ios-arrow-back"
                size={24}
                color={'white'}
                style={{ marginLeft: 15 }}
              />
            </TouchableOpacity>
          ),
          headerRight: (
            <TouchableOpacity onPress={() => navigation.navigate('FormGoals')} style={styles.btnHeader}>
              <Ionicons
                name="ios-add"
                size={24}
                color={'white'}
                style={{ marginRight: 15 }}
              />
            </TouchableOpacity>
          )
        };
      }
    },
    FormGoals: {
      screen: FormGoals,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle : 'New goal',
          headerLeft: (
            <TouchableOpacity onPress={() => navigation.goBack()} style={styles.btnHeader}>
              <Ionicons
                name="ios-arrow-back"
                size={24}
                color={'white'}
                style={{ marginLeft: 15 }}
              />
            </TouchableOpacity>
          )
        };
      }
    },
    DetailGoals: {
      screen: DetailGoals,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle : 'Goal',
          headerLeft: (
            <TouchableOpacity onPress={() => navigation.goBack()} style={styles.btnHeader}>
              <Ionicons
                name="ios-arrow-back"
                size={24}
                color={'white'}
                style={{ marginLeft: 15 }}
              />
            </TouchableOpacity>
          )
        };
      }
    },
    RevenueDivision: {
      screen: RevenueDivision,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle : 'Revenue Division',
          headerLeft: (
            <TouchableOpacity onPress={() => navigation.goBack()} style={styles.btnHeader}>
              <Ionicons
                name="ios-arrow-back"
                size={24}
                color={'white'}
                style={{ marginLeft: 15 }}
              />
            </TouchableOpacity>
          ),
          headerRight: (
            <TouchableOpacity onPress={() => navigation.navigate('EditRevenue')} style={styles.btnHeader}>
              <Ionicons
                name="ios-create"
                size={24}
                color={'white'}
                style={{ marginRight: 15 }}
              />
            </TouchableOpacity>
          )
        };
      }
    },
    EditRevenue: {
      screen: EditRevenue,
      navigationOptions: ({ navigation }) => {
        return {
          headerTitle : 'Edit Revenue',
          headerLeft: (
            <TouchableOpacity onPress={() => navigation.goBack()} style={styles.btnHeader}>
              <Ionicons
                name="ios-arrow-back"
                size={24}
                color={'white'}
                style={{ marginLeft: 15 }}
              />
            </TouchableOpacity>
          )
        };
      }
    },
    FormItem: {
      screen: FormItem,
      navigationOptions: ({ navigation }) => {
        return {
          header : null
        };
      }
    }
  },
  {
    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#002f50'
      },
      headerTintColor: 'white'
    }
  }
);


const AppSwitchNavigator = createSwitchNavigator({
  Loading,
  AuthStackNavigator,
  HomeStackNavigator
});

const AppNavigator = createAppContainer(AppSwitchNavigator);

export default AppNavigator;

const styles = StyleSheet.create({
  btnHeader: {
    width : 44,
    // backgroundColor : 'red'
  }
});