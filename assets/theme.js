import { StyleSheet } from 'react-native';

const colors = {
  logo : '../../assets/logo.png',
  primary : 'red',
  secundary : 'blue',
  primaryGradient : ['#5d5a6b', '#131418'],
}

const theme = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#002f50'
  },
  btnPrimary : {
    // shadowColor: 'rgba(0,0,0, .4)', // IOS
    // shadowOffset: { height: 1, width: 0 }, // IOS
    // shadowOpacity: 1, // IOS
    // shadowRadius: 1, //IOS
    // backgroundColor: 'white',
    elevation: 2, // Android
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderRadius : 30,
    borderWidth : 1,
    // borderColor : '#002540'
    borderColor : 'white'

  }
});

export { colors, theme };

