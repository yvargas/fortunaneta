const initialState = {
  goals: [
    // { key : '001', title : 'Ahorrar', amount : 1000, date : '2019-12-31', done : false, color : 'rgba(255, 105, 97, 0.5)' },
    // { key : '002', title : 'Otra vaina', amount : 5000, date : '2019-12-31', done : true, color : 'rgba(157, 186, 213, 0.5)' },
    // { key : '003', title : 'Rebobinar', amount : 10000, date : '2019-12-31', done : false, color : 'rgba(149, 125, 173, 0.5)' }
  ]
};

const goals = (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD_GOALS':
      return {
        ...state,
        goals: action.payload,
      };
    case 'ADD_GOAL':
      return {
        ...state,
        goals: [action.payload, ...state.goals]
      };
    case 'CHANGE_STATUS':
      return {
         ...state,
        goals: state.goals.map(goal => {
          if (goal.key == action.payload.key) {
            return { ...goal, done: !action.payload.done };
          }
          return goal;
        })
      };
    case 'REMOVE_GOAL':
      return {
        ...state,
        goals: state.goals.filter(goal => goal.key !== action.payload.key),
      };
    default:
      return state;
  }
};

export default goals;
