const initialState = {
  revenue : {
    // basics      : 50, 
    // leisure     : 10, 
    // training    : 10, 
    // charity     : 10, 
    // saving      : 10, 
    // investment  : 10
  }
};

const revenue = (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD_REVENUE':
      return {
        ...state,
        revenue: action.payload,
      };
    case 'UPDATE_DIVISIONS':
      return {
        ...state,
        revenue: action.payload,
      };    
    default:
      return state;
  }
};

export default revenue;
