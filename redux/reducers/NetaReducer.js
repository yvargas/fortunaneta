const initialState = {
  items: [
    // {key : '1' ,category : 'vehicle', title : 'Honda Civic 2012', value : 100000, color: 'rgba(100, 200, 76, 1)'},
    // {key : '2' ,category : 'real-state', title : 'Pantoja', value : 800000, color: 'rgba(131, 167, 234, 1)'},
  ],
};

const items = (state = initialState, action) => {
  switch (action.type) {
    case 'LOAD_ITEMS':
      return {
        ...state,
        items : action.payload,
      };
    case 'ADD_ITEM':
      return {
        ...state,
        items : [action.payload, ...state.items],
      };
    case 'REMOVE_ITEM':
      return {
        state,
        items: state.items.filter(i => i.key !== action.payload.key)
      };
    
    default:
      return state;
  }
};

export default items;
