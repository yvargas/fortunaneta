import { createStore, combineReducers } from 'redux';

import NetaReducer from '../reducers/NetaReducer';
import GoalsReducer from '../reducers/GoalsReducer';
import RevenueDivisionReducer from '../reducers/RevenueDivisionReducer';
import ProfileReducer from '../reducers/ProfileReducer';
const store = createStore(
  combineReducers({
    Neta 		: NetaReducer,
    Revenue 	: RevenueDivisionReducer,
    Goals 		: GoalsReducer,
    Profile		: ProfileReducer
  })
);

export default store;
