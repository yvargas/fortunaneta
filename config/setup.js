export const categories = [
	{
	  label: 'Real State',
	  value: 'real-state',
	},
	{
	  label: 'Vehicle',
	  value: 'vehicle',
	},
	{
	  label: 'Loan',
	  value: 'loan',
	},
	{
	  label: 'Savings',
	  value: 'savings',
	},
	{
	  label: 'Assets',
	  value: 'assets',
	},
];